require 'pessoa'

describe 'Atributos com Let' do
  # before(:each) do
  #   @pessoa = Pessoa.new
  # end

  let(:pessoa) { Pessoa.new }

  it 'have_attributes' do
    pessoa.nome = 'John'
    pessoa.idade = 30

    expect(pessoa).to have_attributes(nome: a_string_starting_with('J'), idade: (be >= 20)) 
  end

  it 'have_attributes' do
    pessoa.nome = 'Jose'
    pessoa.idade = 25

    expect(pessoa).to have_attributes(nome: a_string_starting_with('J'), idade: (be >= 20)) 
  end
end

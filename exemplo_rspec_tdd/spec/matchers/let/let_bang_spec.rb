$count = 0

describe 'let!' do
  order_de_invocacao = []

  let!(:contador) do
    order_de_invocacao << :let!
    $count += 1
  end

  it 'chama o metodo helper antes do teste' do
    order_de_invocacao << :exemplo
    expect(order_de_invocacao).to eq([:let!, :exemplo])
    expect(contador).to eq(1)
  end
end

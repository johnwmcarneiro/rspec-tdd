describe 'Matcher Output' do
  it { expect { puts 'john' }.to output.to_stdout }
  it { expect { print 'john' }.to output('john').to_stdout }
  it { expect { puts 'john'}.to output(/john/).to_stdout }

  it { expect { warn 'john' }.to output.to_stderr }
  it { expect { warn 'john' }.to output("john\n").to_stderr }
  it { expect { warn 'john'}.to output(/john/).to_stderr }
end
require 'rails_helper'

RSpec.describe Customer, type: :model do
  it '#full_name - Overrides attribute' do
    customer = create(:customer, name: 'Fulano dos Santos')
    # customer1 = create(:customer, name: 'Fulano dos Santos')
    # puts customer.email
    # puts customer1.email
    expect(customer.full_name).to eq('Sr. Fulano dos Santos')
  end

  it '#full_name' do
    customer = create(:user) # or create(:customer)
    expect(customer.full_name).to start_with('Sr. ')
  end

  it { expect { create(:customer) }.to change{ Customer.all.size }.by(1) }

  context 'Heranca' do
    it 'customer vip' do
      customer = create(:customer_vip)
      expect(customer.vip).to be_truthy
    end

    it 'customer default' do
      customer = create(:customer_default)
      expect(customer.vip).to be_falsy
    end
  end

  it 'attributes_for' do
    attrs = attributes_for(:customer)
    customer = Customer.create(attrs)
    expect(customer.full_name).to start_with('Sr. ')
  end

  it 'atributo transitorio' do
    customer = create(:customer_default, upcased: true)
    expect(customer.name.upcase).to eq(customer.name)
  end

  it 'Cliente Masculino' do
    customer = create(:customer_male)
    expect(customer.gender).to eq('M')
  end

  it 'Cliente Masculino Vip' do
    customer = create(:customer_male_vip)
    expect(customer.vip).to eq(true)
    expect(customer.gender).to eq('M')
  end

  it 'Cliente Feminino' do
    customer = create(:customer_female)
    expect(customer.gender).to eq('F')
  end

  it 'Cliente Feminino Default' do
    customer = create(:customer_female_default)
    expect(customer.vip).to eq(false)
    expect(customer.gender).to eq('F')
  end

  it 'travel_to' do
    travel_to Time.zone.local(2004, 11, 24, 1, 4, 44) do
      @customer = create(:customer_vip)
    end

    expect(@customer.created_at).to be < Time.now
  end
end
